#!/bin/bash
# Test if synce4l can parse default configuration

set -ex

# Find a nonexistent network device
i=1
while ip link show eth$i &> /dev/null; do
	i=$((i + 1))
done
eth=eth$i

tmpconf=$(mktemp)

sed "s/^\[ens.*\]/\[eth$i\]/" < /etc/synce4l.conf > "$tmpconf"

out=$(timeout -s 9 5 synce4l -m -q -l 7 -f "$tmpconf" 2>&1 || :)

echo "$out" | grep -q "config item synce1"
echo "$out" | grep -q "dpll mon for synce1 new state"

echo "unknownoption 1" >> "$tmpconf"

out=$(timeout -s 9 5 synce4l -m -q -l 7 -f "$tmpconf" 2>&1 || :)

echo "$out" | grep -q "failed to parse configuration file"

rm "$tmpconf"

exit 0
